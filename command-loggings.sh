#!/bin/bash

# Styles definition
STYLE_BOLD="\e[1m"
STYLE_ITALIC="\x1b[3m"
STYLE_UNDERLINED="\x1b[4m"
STYLE_BLINKING="\x1b[5m"
STYLE_INVERTED="\x1b[7m"
STYLE_NORMAL="\e[0m"

# Colors definition
COLOR_NC='\e[0m' # No Color
COLOR_WHITE='\e[1;37m'
COLOR_BLACK='\e[0;30m'
COLOR_BLUE='\e[0;34m'
COLOR_LIGHT_BLUE='\e[1;34m'
COLOR_GREEN='\e[0;32m'
COLOR_LIGHT_GREEN='\e[1;32m'
COLOR_CYAN='\e[0;36m'
COLOR_LIGHT_CYAN='\e[1;36m'
COLOR_RED='\e[0;31m'
COLOR_LIGHT_RED='\e[1;31m'
COLOR_PURPLE='\e[0;35m'
COLOR_LIGHT_PURPLE='\e[1;35m'
COLOR_BROWN='\e[0;33m'
COLOR_YELLOW='\e[1;33m'
COLOR_GRAY='\e[0;30m'
COLOR_LIGHT_GRAY='\e[0;37m'

# Shows a command in green color and then executes it
#
# Input:
# $1 - command line to execute
#
# Output:
# nothing
#
# Example:
# showAndEval "cp file_path1 file_path2"
function showAndEval() {
  DATE=`date '+%F %T %Z'`
  echo -e "${COLOR_CYAN}[$DATE]${COLOR_NC} ${STYLE_BOLD}Executing${STYLE_NORMAL}: ${COLOR_GREEN}$1${COLOR_NC}"
  eval "$1"
}

# Shows a command in red color without executeing it
# Used mostly to see the command syntax in debugging phase
#
# Input:
# $1 - command line to execute
#
# Output:
# nothing
#
# Example:
# showAndNotEval "cp file_path1 file_path2"
function showAndNotEval() {
  DATE=`date '+%F %T %Z'`
  echo -e "${COLOR_CYAN}[$DATE]${COLOR_NC} ${STYLE_BOLD}Fake Executing${STYLE_NORMAL}: ${COLOR_RED}$1${COLOR_NC}"
}

# Given a variable name, it gets its value and shows it in yellow color
# in the format
# >>> NAME: VALUE
#
# Input:
# $1 - variable
#
# Output:
# nothing
#
# Example:
# showVarNameAndValue "VARIABLE_NAME"
function showVarNameAndValue() {
  varName=$1
  DATE=`date '+%F %T %Z'`
  Y=$(eval "echo \$$varName")
  echo -e "${COLOR_CYAN}[$DATE]${COLOR_NC} >>> ${STYLE_BOLD}$varName${STYLE_NORMAL}: ${COLOR_YELLOW}$Y${COLOR_NC}"
}

# Given a separator and an array, returns a string joining the array items using the separator
#
# Input:
# $1 - separator
# $2 - Array
#
# Output:
# string built joined array items with separator
#
# Example:
# join_by ' ' ${list[@]}
# where list is an array
function join_by { local IFS="$1"; shift; echo "$*"; }

# Given a command string with arguments, an argument name and a new value,
# returns the command line with the argument value substitued with the new passed value
#
# Input:
# $1 - Command string
# $2 - Param name which value is to substitute
# $3 - New Param value
#
# Output:
# - The new command string containing the new param value
# - The error code
#
# Error codes:
# 0 - No ERRORS
# 1 - Param not Found
# 2 - Param without value
#
# Example:
# replaceParamValue "do_something -a 2 -m foo -h 4" "m" "bar"
# Returns: do_something -a 2 -m bar -h 4
function replaceParamValue() {
  local inputString=$1 # command string
  local paramName=$2 # Param name to find
  local newParamValue=$3 # Param value to replace
  local retStatus=1 # Return status value
  IFS=' ' read -a list <<< "$inputString"
  total=${#list[*]}
  for i in "${!list[@]}"
  do
    item="${list[$i]}"
    # echo "${list[$i]}"
    if [[ "$item" == "-${paramName}" ]]
      then
        if [[ $i -lt $total-1 ]]
          then
            list[$i+1]=$newParamValue
            retStatus=0
          else
            retStatus=2
        fi
    fi
  done
  echo $(join_by ' ' ${list[@]}) # Return the paramvalue
  return $retStatus # Return the error code
}

# Given a command string with arguments and an argument name,
# returns the value associated with the passed argument name
#
# Input:
# $1 - Command string
# $2 - Param name which value is to substitute
#
# Output:
# - The param value
# - The error code
#
# Error codes:
# 0 - No ERRORS
# 1 - Param not Found
# 2 - Param without value
#
# Example:
# getParamValue "do_something -a 2 -m foo -h 4" "m"
# Returns: foo
function getParamValue() {
  local paramValue='__param_not_found__' # Param value to return
  local inputString=$1 # command string
  local paramName=$2 # Name of the parameter
  local retStatus=1 # Return status value
  IFS=' ' read -a list <<< "$inputString" # Splits the string into an array
  total=${#list[*]} # Number of elements
  for i in "${!list[@]}"
  do
    item="${list[$i]}" # i-th element
    if [[ "$item" == "-${paramName}" ]]
      then
        # Found the parameter
        if [[ $i -lt $total-1 ]]
          then
            # If the param name is not the last item of the array
            paramValue=$(echo "${list[$i +1]}" | sed "s/^'//g"| sed "s/'$//g") # PAram value
            retStatus=0 # Aii right code
          else
            # If the param name is the last item of the array
            paramValue="__param_without_value__" # The param value is not into the command string
            retStatus=2 # Error code
        fi
    fi
  done
  echo $paramValue # Return the paramvalue
  return $retStatus # Return the error code
}

# Given a string to show, prepends it with the formatted datetime in cyan color
# The end line can be changed to any char (i.e. \n,\r). Default is \n
#
# Input:
# $1 - string to show
#
# Output:
# echoes the formatted string
#
# Use example:
# showWithDate "<string>"
function showWithDate() {
  local inputString=$1 # string to show
  local endLineChar=$2 # end line char
  if [[ $endLineChar == '' ]]
  then
    endLineChar="\n"
  fi
  DATE=`date '+%F %T %Z'`
  echo -ne "${COLOR_CYAN}[$DATE]${COLOR_NC} $inputString $endLineChar"
}

# Given in input a number representing a number of seconds,
# returns formatted datetime in "<D>days - <h>h <m>m <s>s"
# where <D> is the number of days, <h> hours and so on
#
# Input:
# $1 - number of seconds to show
#
# Output:
# echoes the formatted string
#
# Use example:
# getFormattedDateTime "<seconds>"
function getFormattedDateTime() {
  local inputTimeString=$1 # time to format
  formattedString=$(printf "%02ddays - %02dh %02dm %02ds\n" "$((inputTimeString/86400))" "$((inputTimeString/3600%24))" "$((inputTimeString/60%60))" "$((inputTimeString%60))")
  echo -ne "$formattedString"
}

# Given a string to show it returns the string formatted as info
#
# Input:
# $1 - string to show
#
# Output:
# echoes the formatted string
#
# Use example:
# logAsInfo "<string to show>"
function logAsInfo() {
  local inputString=$1 # string to show
  showWithDate "${COLOR_LIGHT_BLUE}${STYLE_INVERTED}$inputString${STYLE_NORMAL}${COLOR_NC}"
}

# Given a string to show it returns the string formatted as error
#
# Input:
# $1 - string to show
#
# Output:
# echoes the formatted string
#
# Use example:
# logAsError "<string to show>"
function logAsError() {
  local inputString=$1 # string to show
  showWithDate "${COLOR_RED}${STYLE_INVERTED}$inputString${STYLE_NORMAL}${COLOR_NC}"
}

# Given a string to show it returns the string formatted as success
#
# Input:
# $1 - string to show
#
# Output:
# echoes the formatted string
#
# Use example:
# logAsSuccess "<string to show>"
function logAsSuccess() {
  local inputString=$1 # string to show
  showWithDate "${COLOR_GREEN}${STYLE_INVERTED}$inputString${STYLE_NORMAL}${COLOR_NC}"
}

